# KBParkingAngel
---


## KBParkingAngel은?
---
교보문고 상암동 사옥 주차 할인권 발급 봇!


## Intro
---
  - ** 텔레그램 봇 채널 링크: [입장하기](https://t.me/kbparkinganglebot "KBParkingAngel Bot") **

![KBParkingAngel Intro](KBPA.gif "KBParkingAngel Intro")


### Installing
---
```
git clone https://ddembo4@bitbucket.org/ddembo4/kbparkingangel.git
cd kbparkingangel
pip install -r requirements.txt
python scity_main.py
```

  - ** 실행을 위해 env.dat파일 생성이 필요\[해당 파일은 아래와 같은 변수정의로 이루어짐\] **
```
TOKEN="" # 텔레그램 봇 토큰
CIPHER_KEY="" # 암호화키

KFLOW_TEST='sybase+pyodbc://[ID]:[PW]@[DB NAME]' # 개발DB 접속정보
KFLOW_REAL='sybase+pyodbc://[ID]:[PW]@[DB NAME]' # 운영DB 접속정보

BASE_URL = 'http://a4081.parkingweb.kr'
URI_MAIN = '/main'
URI_LOGIN = '/login/doLogin/'
URI_CARLIST = '/index.php/main/ajax_CarList/'
URI_CARIMAGE = '/index.php/main/ajax_CarImage/'
URI_DISINS = '/index.php/main/ajax_DisIns/'
SPA_ID = "" # 주차권 발급 사이트 계정
SPA_PW = "" # 주차권 발급 사이트 비밀번호

DIVIDER = "!#%!"
```


## Authors
---
  - [KBParkingAngel](https://bitbucket.org/ddembo4/kbparkingangel) - <ddembo4@kyobobook.com>


## License
---
```
CopyLeft (c) All wrongs reserved!
```