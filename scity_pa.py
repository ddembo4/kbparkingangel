#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
*************************************************************************
  1. Title   : PA service for kyobobook center S-City!!
  2. Writer  : hElLoOnG
  3. Date    : 2022.02.25
  4. ETC     : 
  5. History
     - 2022.02.25, helloong, First Created
     - Mod, 2022.06.08, helloong, Fixed bug can't extract 24 hours ticket issued list!
  6. Build Command
     - pyarmor pack -e " -w -F " -x " --exclude venv" scity_pa.py
*************************************************************************
"""
import time
from urllib import parse
import requests
import socket
import re

import scity_logger
import scity_config


class ScityParking:
    logger = scity_logger.MyLogger("scity_main", "scity_main").make_logger("debug")
    sess = None


    def __init__(self):
        self.sess = self.login(scity_config.SPA_ID, scity_config.SPA_PW)
        if not self.sess:
            self.logger.error("로그인에 실패 하였습니다!")
            return None
        else: 
            self.logger.debug("로그인 성공!!")


    def get_ip(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            s.connect(('8.8.8.8', 1))
            ip_addr = s.getsockname()[0]
        except Exception:
            ip_addr = '127.0.0.1'
        finally:
            s.close()
        return ip_addr


    def check_isalive(self) -> bool:
        global SESSION

        r = self.sess.get(''.join([scity_config.BASE_URL, scity_config.URI_MAIN]))
        if r.status_code != 200:
            self.logger.debug("error01!!")
            return False

        html_src = r.text
        chked_val = re.findall("'(\d{2}?:.+?잔여수량\s.+?)'", html_src)
        self.logger.debug(chked_val)
        remain_qty = re.findall("'\d{2}?:.+?잔여수량\s(.+?)'", html_src)
        self.logger.debug(remain_qty)

        if len(chked_val) <= 0:
            SESSION = self.login(scity_config.SPA_ID, scity_config.SPA_PW)
            if not SESSION:
                self.logger.error("로그인에 실패 하였습니다!")
                return False
            else: 
                self.logger.debug("로그인 성공!!")
                return True
        else:
            return True

    def login(self, id: str = None, pw: str = None):
        if not id:
            return None
        if not pw:
            return None

        payload = {
            'login_id': id,
            'login_pw': pw,
            'login_ip': self.get_ip(),
            'is_ajax': '1',
        }

        s = requests.Session()
        s.headers.update(scity_config.COMMON_HEADERS)
        self.logger.debug(s.headers)
        try:
            r = s.post(''.join([scity_config.BASE_URL, scity_config.URI_LOGIN]), data=payload)
            if r.status_code != 200:
                self.logger.debug("error01!!")
                return None

            retval = r.json()
            if retval['result'] != 'success':
                self.logger.debug("error02!!")
                return None

            get_cookie = r.cookies.get_dict()
            self.logger.debug(f"login finished[{get_cookie}]!!")
            s.headers.update()

            return s
        except Exception as e:
            return None


    def get_return_code(self):
        r = self.sess.get(''.join([scity_config.BASE_URL, scity_config.URI_MAIN]))
        if r.status_code != 200:
            self.logger.debug("error01!!")
            return None

        html_src = r.text
        return_cd = re.findall("match\(\"(.+)?\"\).+\s+.+?alert\('(.+?)'", html_src)
        self.logger.debug(return_cd)


    def get_main(self):
        global SESSION

        err_cnt = 0

        while True:
            r = self.sess.get(''.join([scity_config.BASE_URL, scity_config.URI_MAIN]))
            if r.status_code != 200:
                self.logger.debug("error01!!")
                return None

            html_src = r.text
            chked_val = re.findall("'(\d{2}?:.+?잔여수량\s.+?)'", html_src)
            self.logger.debug(chked_val)
            remain_qty = re.findall("'\d{2}?:.+?잔여수량\s(.+?)'", html_src)
            self.logger.debug(remain_qty)

            if len(chked_val) <= 0:
                err_cnt += 1

                SESSION = self.login(scity_config.SPA_ID, scity_config.SPA_PW)
                if not SESSION:
                    self.logger.error("로그인에 실패 하였습니다!")
                    return None
                else: 
                    self.logger.debug("로그인 성공!!")
                
                if err_cnt >= 3:
                    self.logger.error(f"주차권 잔여수량 수집 실패!![HTML_SRC:\n{html_src}\n]")
                    break
            else:
                break

        retval = []
        for items in zip(chked_val, remain_qty):
            retval.append(items)

        self.logger.debug(f"retval: {retval}")
        return retval


    def get_car_list(self, car_num: str = None):
        if not car_num:
            self.logger.debug("error!!")
            return None

        payload = {
            'carNumber': car_num,
            'is_ajax': '1',
        }

        r = self.sess.post(''.join([scity_config.BASE_URL, scity_config.URI_CARLIST]), data=payload)
        if r.status_code != 200:
            self.logger.debug("error01!!")
            return None

        retval = r.text
        filtered_retval = re.findall("fnCarInfo\('(.+?)',\s'(.+?)',\s'(.+?)',\s'(.+?)'\);", retval)
        self.logger.debug(filtered_retval)
        return filtered_retval


    def get_car_image(self, car_info: tuple = None):
        if not car_info:
            self.logger.debug("error!!")
            return None
        # car_info = ('156호1988', '2022-02-15 13:27:13', '236 시간 33 분 ', '0011022021513271440001')
        payload = {
            'carNumber': car_info[0],
            'TKNo': car_info[3],
            'InTime': car_info[1],
            'is_ajax': '1',
        }

        r = self.sess.post(''.join([scity_config.BASE_URL, scity_config.URI_CARIMAGE]), data=payload)
        if r.status_code != 200:
            self.logger.debug("error01!!")
            return None

        self.logger.debug(r.text)
        return r.text


    def send_disins(self, payload: dict = None):
        if not payload:
            self.logger.debug("error!!")
            return None

        r = self.sess.post(''.join([scity_config.BASE_URL, scity_config.URI_DISINS]), data=payload)
        if r.status_code != 200:
            self.logger.debug("error!!")
            return None

        retval = r.text
        if retval in scity_config.RETCODE_MAP.keys():
            return [retval, scity_config.RETCODE_MAP[retval]]
        else:
            return [retval, '알 수 없는 응답코드!!']


    def put_car_disins(self, applied_time: int = None, car_info: tuple = None):
        if not applied_time:
            return [-101, "할인시간 입력 오류!"]
        if not car_info:
            return [-102, "입차 정보 입력 오류!"]

        # 02. 주차권 잔량
        # ticket_remain = [('41:1시간무료(웹) / 잔여수량 999885', '999885'), ('61:1시간유료(웹) / 잔여수량 314', '314'), ('63:2시간유료(웹) / 잔여수량 101', '101'), ('75:24시간유료(웹) / 잔여수량 46', '46')]
        ticket_remain = self.get_main()
        tot_hours = 1
        tot_hours += int(ticket_remain[1][1]) * 1
        tot_hours += int(ticket_remain[2][1]) * 2
        # tot_hours += int(ticket_remain[3][1]) * 24 # 24시간 일권은 할인가능 주차 시간에 누적 합산하지 않음

        if tot_hours < applied_time:
            return [-100, '주차권부족']

        payload = {
            'carNumber': car_info[0],
            'chkedVal': '',
            'TKNo': car_info[3],
            'InTime': car_info[1],
            'Reserve4': '',
            'is_ajax': '1',
        }

        if applied_time >= 24:
            # 24시간보다 크거나 같으면 일권을 넣는다!!
            payload['chkedVal'] = ticket_remain[3][0]
            retval = self.send_disins(payload)

            if retval[0].strip() != 'success':
                return [-1, f'24시간권 지급 실패!![에러코드:{retval[0]}, {retval[1]}]']
        else:
            # 무조건 1시간은 무료 시간을 넣어본다!
            payload['chkedVal'] = ticket_remain[0][0]
            retval = self.send_disins(payload)

            if retval[0].strip() == 'success':
                applied_time -= 1
            elif retval[0].strip() == 'FreeCoupon':
                self.logger.debug(f'무료권 기 지급 완료 차량!![에러코드:{retval[0]}, {retval[1]}]')
            else:
                self.logger.debug(f'알 수 없는 응답코드![{retval[0]}')
                return [-1, f'무료권 지급 실패!![에러코드:{retval[0]}, {retval[1]}]']

            loop_cnt = 0
            ticket_try_flag = {
                '1': True,
                '2': True,
            }

            while applied_time >= 1:
                time.sleep(0.1)
                self.logger.debug(f"주차 지급 시간 잔량: {applied_time}")

                ticket_remain = self.get_main()

                loop_cnt += 1
                # 1. loop_cnt 10 이상은 돌지 않는다!!
                self.logger.debug(f'지급시도 횟수: {loop_cnt}')
                if loop_cnt >= 10:
                    break

                # 1. 유료 1시간 티켓을 가능 할때까지 넣는다!
                if ticket_try_flag['1']:
                    if int(ticket_remain[1][1]) > 0:
                        payload['chkedVal'] = ticket_remain[1][0]
                        retval = self.send_disins(payload)

                        if retval[0].strip() == 'success':
                            applied_time -= 1
                            continue
                        else:
                            ticket_try_flag['1'] = False
                            continue
                    else:
                        ticket_try_flag['1'] = False
                        continue
                else:
                    self.logger.debug('1시간권 없음!!')

                # 2. 1시간권이 없거나 불가능하면 2시간권을 넣는다!
                if ticket_try_flag['2']:
                    if int(ticket_remain[2][1]) > 0:
                        payload['chkedVal'] = ticket_remain[2][0]
                        retval = self.send_disins(payload)

                        if retval[0].strip() == 'success':
                            applied_time -= 2
                            continue
                        else:
                            ticket_try_flag['2'] = False
                            continue
                    else:
                        ticket_try_flag['2'] = False
                        continue
                else:
                    self.logger.debug('2시간권 없음!!')

                # 3. 여기까지 왔다면 더이상 넣을 가용한 주차권이 없는 경우!
                self.logger.debug('여긴 오지 말아야 함!![더이상 지급가능 주차권 없음]')
                return [applied_time, "지급 가능 주차권 없음"]
            
            if applied_time <= 0:
                return [applied_time, "처리완료"]
            else:
                return [applied_time, "처리완료[주차권 부족]"]


    def get_report(self, from_date: str = '', to_date: str = '', car_no: str = ''):
        report_url = scity_config.BASE_URL + f"/report/ajax_DisList?selMode=0&frDate={from_date}&frTime=00:00&toDate={to_date}&toTime=23:00&carNo={car_no}"
        self.logger.debug(f"report_url: {report_url}")

        r = self.sess.get(report_url)
        if r.status_code != 200:
            self.logger.debug("error01!!")
            return None

        html_src = r.text
        # self.logger.debug(html_src)
        # Mod, 2022.06.08, helloong, Fixed bug can't extract 24 hours ticket issued list!
        re_find = re.findall("<td align=\"Center\">(\d{2,3}.+?\d{4})</td>.+?(\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2})</td>[\s\t]<td align='Center'>(\d{1,2}시간.+?)\(웹\)</td>[\s\t]+?<td align='Center'>", html_src)
        self.logger.debug(re_find)

        return re_find

if __name__ == '__main__':
    SPA = ScityParking()

    SPA.get_report('2022-06-08', '2022-06-08', '1412')
"""
    # 01. 로그인
    SESSION = SPA.login('s교보', '1900')
    if not SESSION:
        sys.exit(-1)

    # 02. 주차권 잔량
    # ticket_remain = [('41:1시간무료(웹) / 잔여수량 999885', '999885'), ('61:1시간유료(웹) / 잔여수량 314', '314'), ('63:2시간유료(웹) / 잔여수량 101', '101'), ('75:24시간유료(웹) / 잔여수량 46', '46')]
    # ticket_remain = get_main(SESSION)

    # 03. 차량조회
    car_num = input("차량 뒷 4자리 번호 입력: ")

    ret_car_list = SPA.get_car_list(SESSION, car_num)
    self.logger.debug("############################################################")
    self.logger.debug("######################입차 차량 리스트######################")
    self.logger.debug("############################################################")
    for idx, car_list in enumerate(ret_car_list):
        self.logger.debug(f"[{idx + 1}]. {car_list[0]}")
    
    if len(ret_car_list) > 1:
        car_info_idx = input("리스트중 주차권 지급 대상 차량의 번호를 선택하여 주세요![중단: q]")
        if not car_info_idx:
            self.logger.debug("선택된 차량이 없습니다!!")
            sys.exit(-1)
    else:
        car_info_idx = 1
    
    car_info = ret_car_list[car_info_idx - 1]

    # 04. 차량 이미지 조회
    SPA.get_car_image(SESSION, car_info)
    
    # 05. 주차 시간 입력
    applied_time = input("적용 주차 시간(1시간단위) 입력[숫자]: ")

    # 06. 할인 등록
    retval = SPA.put_car_disins(SESSION, int(applied_time), car_info)

    self.logger.debug(f"요청시간: {applied_time}, 미충전 잔량:{retval[0]}")

"""