#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
*************************************************************************
  1. Title   :
  2. Writer  : hElLoOnG
  3. Date    :
  4. ETC     :
  5. History
     - 2022.03.06, helloong, First Created
*************************************************************************
"""

QUERY_MAPS = {
    # Add, 2022.03.06, 심윤보, 인증 유저 여부 확인
    "findIsAuthedUser": """
      SELECT code_id, code_wrth, seq, code_wrth_name, code_id_dscr, etc1, etc2, crtr_id, cret_dttm, amnr_id, amnd_dttm, dlt_ysno
        FROM IDSS..TC_STR_CODE
      WHERE code_id = '0026'
        AND code_wrth_name = '{cid}'
          AT ISOLATION 0
    """,

    # Add, 2022.03.07, 심윤보, 기등록 유저 검색
    "isAlreadyExistsUser": """
      SELECT IsNull(ltrim(rtrim(dlt_ysno)), '') AS dlt_ysno
        FROM IDSS..TC_STR_CODE T1
      WHERE T1.code_id        = '0026'
        AND T1.code_wrth_name = '{cid}'
          AT ISOLATION 0
    """,

    # Add, 2022.03.07, 심윤보, 인증 요청 유저 등록
    "requestUserAuth": """
      DECLARE @codeWrth CHAR(3), @seq NUMERIC(20, 0)

      SELECT @codeWrth = str_replace(str(convert(NUMERIC(3, 0), max(T1.code_wrth)) + 1, 3), ' ', '0')
        FROM IDSS..TC_STR_CODE T1
      WHERE T1.code_id = '0026'
          AT ISOLATION 0
          
      SELECT @seq = IsNull(max(T1.seq), 0) + 1
        FROM IDSS..TC_STR_CODE T1
      WHERE T1.code_id = '0026'
          AT ISOLATION 0

      INSERT INTO IDSS..TC_STR_CODE (code_id, code_wrth, seq, code_wrth_name, code_id_dscr, etc1, etc2, crtr_id, cret_dttm, amnr_id, amnd_dttm, dlt_ysno)
      VALUES ('0026', @codeWrth, @seq, '{cid}', 'ParkingAngleAUTH', '{contact}', '{username}', '12570', getdate(), '12570', getdate(), 'Y')

      SELECT count(*) AS cnt
        FROM IDSS..TC_STR_CODE T1
      WHERE T1.code_id        = '0026'
        AND T1.code_wrth      = @codeWrth
        AND T1.code_wrth_name = '{cid}'
          AT ISOLATION 0
    """,

    # Add, 2022.03.07, 심윤보, 사용요청 유저 승인
    "updateApprovedUserAuth": """
      UPDATE IDSS..TC_STR_CODE
        SET dlt_ysno  = 'N'
          , amnd_dttm = getdate()
      WHERE code_id        = '0026'
        AND code_wrth_name = '{cid}'

      SELECT count(*)         AS cnt
           , IsNull(etc2, '') AS etc2
        FROM IDSS..TC_STR_CODE T1
      WHERE T1.code_id        = '0026'
        AND T1.code_wrth_name = '{cid}'
        AND T1.dlt_ysno       = 'N'
      GROUP BY etc2
          AT ISOLATION 0
    """,

    # Add, 2022.03.07, 심윤보, 사용요청 유저 거절
    "deleteApprovedUserAuth": """
      DELETE IDSS..TC_STR_CODE
       WHERE code_id        = '0026'
         AND code_wrth_name = '{cid}'

      SELECT count(*)         AS cnt
        FROM IDSS..TC_STR_CODE T1
       WHERE T1.code_id        = '0026'
         AND T1.code_wrth_name = '{cid}'
          AT ISOLATION 0
    """,

    # Add, 2020.05.12, 심윤보, 키오스 바로드림 매출처리 프로시저 호출
    "registerSaleProcessProcedures": """
      DECLARE @dlvrRequId CHAR(13), @dlvrRequIemSrmb INT
      EXEC EBIO.dbo.usp_dlvr_sls_pros_snms '{dlvrRequId}', {dlvrRequIemSrmb}
      
      SELECT 0 AS retCode
    """,
}

