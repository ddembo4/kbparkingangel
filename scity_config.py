# -*- coding: utf-8 -*-

"""
    Title:
    Author: helloong@gmail.com
    DateTime: 0000.00.00 First created
    History:
        -
"""
from dotenv import load_dotenv
import os
import sys

extDataDir = os.getcwd()
if getattr(sys, 'frozen', False):
    extDataDir = sys._MEIPASS

# Credentials
load_dotenv(dotenv_path=os.path.join(extDataDir, 'env.dat'))

SERVICE_NAME = 'KBPARKINGANGLE'
ADMIN_ID_LIST = ['287456840', ]

TOKEN = os.getenv('TOKEN')
CIPHER_KEY = os.getenv('CIPHER_KEY')

# KFLOW_TEST
# KFLOW = os.getenv('KFLOW_TEST')

# KFLOW_REAL
KFLOW = os.getenv('KFLOW_REAL')

BASE_URL = os.getenv("BASE_URL")
URI_MAIN = os.getenv("URI_MAIN")
URI_LOGIN = os.getenv("URI_LOGIN")
URI_CARLIST = os.getenv("URI_CARLIST")
URI_CARIMAGE = os.getenv("URI_CARIMAGE")
URI_DISINS = os.getenv("URI_DISINS")
SPA_ID = os.getenv("SPA_ID")
SPA_PW = os.getenv("SPA_PW")

DIVIDER = os.getenv("DIVIDER")

COMMON_HEADERS = {
    'Host': 'a4081.parkingweb.kr',
    'Connection': 'keep-alive',
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'X-Requested-With': 'XMLHttpRequest',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36 Edg/98.0.1108.56',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'Origin': 'http://a4081.parkingweb.kr',
    'Referer': 'http://a4081.parkingweb.kr/index.php/login',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'ko,en;q=0.9,en-US;q=0.8',
}

RETCODE_MAP = {
    'LoginFail': '로그인세션이 종료되었습니다. 로그인을 다시 해주세요.',
    'DCCount': '이미 할인처리 되었습니다.',
    'bigoFail': '비고가 필수사항입니다.',
    '-99': '할인 실패! 관리자에게 문의해 주세요.',
    '-98': '할인한도를 초과하였습니다.',
    'success': '할인처리 되었습니다.',
    '0': '할인 실패! 관리자에게 문의해 주세요.',
    '-1': '할인권을 발매 후 사용해 주세요.',
    '-2': '사용가능한 수량이 없습니다. ',
    'CountOver1': '이미 해당 업체에서 할인받은 차량입니다.',
    'CountOver2': '이미 할인받은 차량입니다.',
    'FreeCoupon': '무료권은 한번만 가능합니다.',
    'LoginFail': '로그인세션이 종료되었습니다. 로그인을 다시 해주세요.',
    'groupFail': '현재 업체가 그룹정보가 등록되어있지 않습니다.',
    'groupCntFail': '할인 실패. 무료권 사용횟수 초과.',
    'payCntFail': '할인 실패. 유료권 사용횟수 초과.',
    'overGroupCnt': '할인 실패. 현재 업체와 동일한 군에서 이미 할인 받았습니다.',
    'overGroupUseCnt': '할인 실패. 사용횟수가 초과되었습니다.',
    'overGroupDayCnt': '할인 실패. 사용횟수가 초과되었습니다.',
    'kakaoFail': '할인 실패. 현재 할인 등록하려는 차량은 카카오에 이미 등록된 차량입니다.',
    'storeMaxFail': '할인 실패. 할인 가능한 최대 업체 수를 초과했습니다.',
    'permissionFail': '할인 실패. 해당 차량은 할인 가능한 주차구역을 위반하였습니다.',
}


TIMEOUT = 30

if __name__ == '__main__':
    print('hElLoOnG Wolrd!!')
