# -*- coding: utf-8 -*-

"""
*************************************************************************
  1. Title   : DataBase connection for KB Store PDA Server Daemon
  2. Author  : hElLoOnG
  3. Date    : 2021.02.26
  4. ETC     :
  5. Build   :
  6. History
    - 2021.02.26, helloong, First Created
*************************************************************************
"""
import sqlalchemy
from sqlalchemy import event
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session

import scity_config
import scity_logger

KFLOW = scity_config.KFLOW
TIMEOUT = scity_config.TIMEOUT


LOGGER = scity_logger.MyLogger("scity_main", "scity_main").merge_logger("sqlalchemy.engine", "scity_main")
KFLOW_ENGINE = sqlalchemy.create_engine(KFLOW, encoding='utf8', echo=True, pool_size=20, max_overflow=50, connect_args={'CommandTimeOut': TIMEOUT})
KFLOW_SESSION = scoped_session(sessionmaker(bind=KFLOW_ENGINE, autocommit=False))


def get_db_session(server='KFLOW'):
    if server.upper() == 'KFLOW':
        temp_db_engine = sqlalchemy.create_engine(KFLOW, encoding='utf8', echo=True, pool_size=20, max_overflow=50,
                                                connect_args={'CommandTimeOut': TIMEOUT})
        temp_session = sessionmaker(bind=temp_db_engine, autocommit=False)
        return temp_db_engine, temp_session
