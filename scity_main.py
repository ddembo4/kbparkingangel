#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
*************************************************************************
  1. Title   : ParkingAngle service for KB S-City!!
  2. Writer  : hElLoOnG
  3. Date    : 2022.03.04
  4. ETC     : 
  5. History
     - 2022.03.04, helloong, First Created
     - 2022.03.10, helloong, Mod, Fixed msg delete error in register_next_step_handler!
     - 2022.03.21, helloong, Add, Showing remain tickets count at the time of register free tickets!
     - 2022.03.26, helloong, Mod, Fixed an error when show a result message at the end of the task.
     - 2022.06.08, helloong, Mod, Fixed bug None type return value error when issue 24 hours ticket.
  6. Build Command
     - pyarmor pack -e " -c -F " -x " --exclude venv" scity_main.py
     - pyinstaller -c -F -i=icon.ico --hidden-import sqlalchemy --hidden-import requests --hidden-import telebot --hidden-import dotenv --hidden-import pyodbc --add-binary="env.dat;." scity_main.py
*************************************************************************
"""
from ast import parse
import sys
import json
from datetime import datetime
from collections import defaultdict

import telebot
from telebot import util, types
from sqlalchemy.orm import Session

import scity_logger
import scity_config
import scity_pa
from scity_query_map import QUERY_MAPS
from scity_database import KFLOW_SESSION

BOT = telebot.TeleBot(scity_config.TOKEN)
LOGGER = scity_logger.MyLogger("scity_main", "scity_main").make_logger("debug")

SERVICE_NAME = scity_config.SERVICE_NAME
ADMIN_ID_LIST = scity_config.ADMIN_ID_LIST
CIPHER_KEY = scity_config.CIPHER_KEY
TIMEOUT = scity_config.TIMEOUT

SENDPHONE = types.ReplyKeyboardMarkup(one_time_keyboard=True)
AGGREBTN = types.KeyboardButton("동의", request_contact=True)
DISAGGREBTN = types.KeyboardButton("거부")
SENDPHONE.add(AGGREBTN)
SENDPHONE.add(DISAGGREBTN)

PARKINGSET = types.InlineKeyboardMarkup(row_width=2)
REGBTN = types.InlineKeyboardButton("주차등록", callback_data="reg_parking")
SHOWBTN = types.InlineKeyboardButton("주차확인", callback_data="show_parking")
PARKINGSET.add(REGBTN, SHOWBTN)

CANCELSET = types.InlineKeyboardMarkup()
CANCELBTN = types.InlineKeyboardButton("취소", callback_data="btn_cancel")
CANCELSET.row(CANCELBTN)

HIDEKEYBOARD = types.ReplyKeyboardRemove()

GLOBAL_PARAMS = defaultdict(dict)
DIVIDER = scity_config.DIVIDER

USER_INFO = defaultdict(dict)

HD = f"<pre><code class=\"language-python\">"
FT = f"</code></pre>"

SPA = scity_pa.ScityParking()


# CODE AND FUNCTION #
def exec_db(db: Session = KFLOW_SESSION(), query_map_name=None, cond=dict()):
    """
    Add, 2020.05.12, 심윤보, DB 쿼리를 실행하고 그 결과를 이쁘게 알려준다.
    :param db: DB세션
    :param query_map_name: 쿼리스트링
    :param cond:
    :return: 리스트형 처리결과 + 쿼리
    """
    query_str = QUERY_MAPS[query_map_name].format(**cond)
    result = db.execute(query_str)
    result_set = []
    for row in result:
        result_set.append(dict(zip(result.keys(), row)))

    return result_set, query_str


def write_db(db: Session = KFLOW_SESSION(), write_query_map_name=None, check_query_map_name=None, cond=dict()):
    query_str = QUERY_MAPS[write_query_map_name].format(**cond)
    db.execute(query_str)
    result_set = exec_db(db, check_query_map_name, cond)
    return result_set[0], query_str


def make_ret_value(ret_code, ret_msg, err_code, ret_data_count, ret_data, sess_list, query_str="", ip_adrs=""):
    """
    Add, 2020.05.12, 심윤보, 응답 와꾸에 맞게 데이터를 이쁘게 배열한 뒤 제이슨 형태로 만들어가지구능 더 이쁘게 리턴한다.
    :param ret_code: 응답코드
    :param ret_msg: 응답메세지
    :param err_code: 에러코드
    :param ret_data_count: ret_data의 길이
    :param ret_data: 응답데이터
    :param sess_list: 세션 리스트
    :param query_str: 쿼리스트링
    :param ip_adrs: 요청아이피
    :return:
    """
    result_set = dict()
    result_set["retCode"] = ret_code
    result_set["retMsg"] = ret_msg
    result_set["errCode"] = err_code
    result_set["retDataCount"] = ret_data_count
    if type(ret_data) != list:
        ret_data = [ret_data]
    result_set["retData"] = ret_data
    result_set["reqIpAdrs"] = ip_adrs

    if ret_code != 0:
        # 01. 에러 메세지 전송
        # sendmsg(CHAT_ID_LIST, result_set)
        LOGGER.error(f"{ret_msg}")
        LOGGER.error(f"[QUERY]: {query_str}")

        # 02. 세션 롤백
        for sess_item in sess_list:
            LOGGER.error("세션 롤백 후 종료")
            sess_item.rollback()
            sess_item.close()
    else:
        LOGGER.debug("전체 세션 커밋 후 종료")
        for sess_item in sess_list:
            sess_item.commit()
            sess_item.close()

    return result_set


def send_msg_to_admin(msg=None, file=None):
    if not msg:
        return

    sendmsg(ADMIN_ID_LIST, msg, file)


def listener(messages):
    """
    When new messages arrive TeleBot will call this function.
    """
    for m in messages:
        # LOGGER.debug(f"Listener: {m}")
        if m.content_type == "text":
            pass


def stop_service():
    LOGGER.debug(u"Parking Angle Service Stopped!")
    BOT.stop_polling()
    sys.exit(0)


def telegram_polling():
    LOGGER.debug("Parking Angle Service Starting...")
    try:
        # BOT.set_update_listener(listener)
        BOT.infinity_polling(timeout=180)
    except Exception as e:
        LOGGER.exception("An error occured during polling bot!!")
    finally:
        stop_service()


def sendmsg(id_list, msg, file_name=None):
    """
    Add, 2020.05.12, 심윤보, 텔레그램 메세지를 이쁘게 보내준다.
    :param chat_id: 리스트형 채팅 아이디
    :param msg: 보낼메세지
    :param query_str: 쿼리가 있다면 쿼리[파일로 전송]
    :return: 그른거 음따.
    """
    LOGGER.debug(f"sendmsg.id_list: {id_list}, msg: {msg}")
    try:
        if type(msg) == dict:
            msg_str = json.dumps(msg, sort_keys=True, indent=4, ensure_ascii=False)
        elif type(msg) == str:
            msg_str = msg

        if len(msg_str) > 3000:
            spt_msg_str = util.split_string(msg_str, 3000)
        else:
            spt_msg_str = msg_str

        if type(spt_msg_str) != list:
            spt_msg_str = [spt_msg_str, ]

        for chat_id in id_list:
            for msg_item in spt_msg_str:
                try:
                    BOT.send_message(chat_id, HD + msg_item + FT)
                    time.sleep(0.1)
                    if file_name:
                        with open(file_name, 'rb') as fr:
                            doc = fr
                            BOT.send_document(id, doc)
                except Exception as e:
                    LOGGER.exception(f"메세지 전송 실패!![{chat_id}]")

    except Exception as e:
        LOGGER.exception(f'메세지 발송 에러: \nchat_id: {chat_id}\nmsg: {msg}\nerrmsg: {e}')
        pass


def is_authed_user(cid) -> bool:
    LOGGER.debug(f"is_authed_user.cid: {cid}")

    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]
    
    cond = {
        "cid": str(cid),
    }

    result_set, query_str = exec_db(kflow_sess, "findIsAuthedUser", cond)
    LOGGER.debug(f"result_set: {result_set}")
    _ = make_ret_value(0, f"정상처리 완료", 0, len(result_set), result_set, session_list, query_str, "")

    if len(result_set) <= 0:
        return False
    if result_set[0]['dlt_ysno'] != 'N':
        return False
    
    return True


def service_init(cid: int, uname: str):
    if is_authed_user(str(cid)):
        BOT.send_message(cid, f"[{uname}]님 원하시는 작업을 선택 하여주세요.", parse_mode="HTML", reply_markup=PARKINGSET)
    else:
        msg = BOT.send_message(cid, "인증전에는 명령이나 메세지를 입력할 수 없습니다.", reply_markup=SENDPHONE)
        BOT.register_next_step_handler(msg, user_auth_process_step)
        return


def user_auth_check(cid):
    if not is_authed_user(cid):
        msg = BOT.send_message(cid, "인증전에는 명령이나 메세지를 입력할 수 없습니다.", reply_markup=SENDPHONE)
        BOT.register_next_step_handler(msg, user_auth_process_step)
        return False
    
    return True


def remove_msg(cid: int = -1, mid: int = -1) -> bool:
    LOGGER.debug(f"remove_msg.cid: {cid}, remove_msg.mid: {mid}")
    if cid == -1 or mid == -1:
        LOGGER.error(f"remove_msg 파라메터 입력 오류!")
        return
    retval = False

    try:
        retval = BOT.delete_message(cid, mid)
    except telebot.apihelper.ApiTelegramException as e:
        LOGGER.debug(f"cid:{cid}, mid: {mid} Message delete error!!")
    
    return retval 

@BOT.callback_query_handler(func=lambda call: call.data[0:call.data.find(DIVIDER)] == "cancel_auth_user")
def cancel_auth_user(call):
    LOGGER.debug(f"cancel_auth_user.call: {call}")
    cid = call.message.chat.id
    uid = call.from_user.id
    mid = call.message.message_id

    params = call.data.split(DIVIDER)
    lnline_name = params[0]
    args = params[1:]

    LOGGER.debug(lnline_name)
    LOGGER.debug(args)

    param_cid = args[0]
    
    LOGGER.debug(f"cid: {cid}, param_cid: {param_cid}")
    # 인라인 실행시 해당 메세지를 삭제한다.(편의에 따라 추가 삭제)
    retval = remove_msg(cid, mid)
    LOGGER.debug(f"메세지삭제[{cid}/{mid}]: {retval}")

    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]
    
    cond = {
        "cid": str(param_cid),
    }

    result_set, query_str = exec_db(kflow_sess, "deleteApprovedUserAuth", cond)
    LOGGER.debug(f"result_set: {result_set}")

    if result_set[0]['cnt'] != 0:
        LOGGER.debug(f"[{cond}]유저 인증 삭제 실패!![result_set: {result_set}]")
        send_msg_to_admin(f"[{cond}]유저 인증 삭제 실패!![result_set: {result_set}]")
        _ = make_ret_value(-1, f"[{cond}]유저 인증 삭제 실패!![result_set: {result_set}]", 100, len(result_set), result_set, session_list, query_str, "")
        return

    _ = make_ret_value(0, f"정상처리 완료", 0, len(result_set), result_set, session_list, query_str, "")

    BOT.send_message(int(param_cid), f"관리자로부터 사용 승인이 거절 되었습니다! 안녕히 가세요!")


@BOT.callback_query_handler(func=lambda call: call.data[0:call.data.find(DIVIDER)] == "proc_auth_user")
def proc_auth_user(call):
    LOGGER.debug(f"proc_auth_user.call: {call}")
    cid = call.message.chat.id
    uid = call.from_user.id
    mid = call.message.message_id

    params = call.data.split(DIVIDER)
    lnline_name = params[0]
    args = params[1:]

    LOGGER.debug(lnline_name)
    LOGGER.debug(args)

    param_cid = args[0]
    
    LOGGER.debug(f"cid: {cid}, param_cid: {param_cid}")
    # 인라인 실행시 해당 메세지를 삭제한다.(편의에 따라 추가 삭제)
    retval = remove_msg(cid, mid)
    LOGGER.debug(f"메세지삭제[{cid}/{mid}]: {retval}")

    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]
    
    cond = {
        "cid": str(param_cid),
    }

    result_set, query_str = exec_db(kflow_sess, "updateApprovedUserAuth", cond)
    LOGGER.debug(f"result_set: {result_set}")

    if len(result_set) <= 0 or result_set[0]['cnt'] != 1:
        LOGGER.debug(f"[{cond}]유저 인증 승인 실패!![result_set: {result_set}]")
        send_msg_to_admin(f"[{cond}]유저 인증 승인 실패!![result_set: {result_set}]")
        _ = make_ret_value(-1, f"[{cond}]유저 인증 승인 실패!![result_set: {result_set}]", 100, len(result_set), result_set, session_list, query_str, "")
        return

    _ = make_ret_value(0, f"정상처리 완료", 0, len(result_set), result_set, session_list, query_str, "")

    BOT.send_message(int(param_cid), f"관리자로부터 사용 승인이 완료 되었습니다! 지금 즉시 사용 가능합니다!")
    BOT.send_message(int(param_cid), f"[{result_set[0]['etc2']}]님 원하시는 작업을 선택 하여주세요.", parse_mode="HTML", reply_markup=PARKINGSET)


def make_auth_request_btn(cid):
    LOGGER.debug(f"make_auth_request_btn.cid: {cid}")

    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]
    
    cond = {
        "cid": str(cid),
    }

    result_set, query_str = exec_db(kflow_sess, "findIsAuthedUser", cond)
    LOGGER.debug(f"result_set: {result_set}")
    _ = make_ret_value(0, f"정상처리 완료", 0, len(result_set), result_set, session_list, query_str, "")

    if len(result_set) <= 0:
        LOGGER.debug(f"[{cond}]존재하지 않는 유저!! 인증시도 오류!!")
        send_msg_to_admin(f"[{cond}]존재하지 않는 유저!! 인증시도 오류!!")
        return
    
    noti_msg = f"""
사용자 승인 요청
CHATID: {result_set[0]['code_wrth_name']}
전화번호: {result_set[0]['etc1']}
사용자명: {result_set[0]['etc2']}
    """

    cmd_inline_btn = types.InlineKeyboardMarkup(row_width=2)
    btn_auth_user = types.InlineKeyboardButton(f"승인", callback_data=f"proc_auth_user{DIVIDER}{str(cid)}")
    btn_calcel_auth_user = types.InlineKeyboardButton(f"거절", callback_data=f"cancel_auth_user{DIVIDER}{str(cid)}")
    cmd_inline_btn.add(btn_auth_user, btn_calcel_auth_user)

    for mem_cid in ADMIN_ID_LIST:
        retval = BOT.send_message(mem_cid, HD + noti_msg + FT, parse_mode='HTML', reply_markup=cmd_inline_btn)


def user_auth_process_step(m):
    global SENDPHONE, HIDEKEYBOARD
    LOGGER.debug(f"user_auth_process_step.m: {m}")
    cid = m.chat.id
    uname = m.chat.username
    text = m.text
    LOGGER.debug(f"cid: {cid}, text: {text}")

    if m.text == "거부":
        msg = BOT.send_message(cid, "인증전에는 명령이나 메세지를 입력할 수 없습니다.", reply_markup=SENDPHONE)
        BOT.register_next_step_handler(msg, user_auth_process_step)
        return

    try:
        contact = m.contact.phone_number

        BOT.send_message(cid, "사용자 인증 요청중입니다.", reply_markup=HIDEKEYBOARD)
        LOGGER.debug(f"cid: {cid}, contact: {contact}, text: {text}")

        kflow_sess = KFLOW_SESSION()
        session_list = [kflow_sess, ]

        cond = {
            "cid": str(cid),
            "contact": str(contact),
            "username": uname,
        }

        result_set, query_str = exec_db(kflow_sess, 'isAlreadyExistsUser', cond)
        LOGGER.debug(f"result_set: {result_set}")
        _ = make_ret_value(0, f"정상처리 완료", 0, len(result_set), result_set, session_list, query_str, "")
        if len(result_set) > 0:
            if result_set[0]['dlt_ysno'] == 'Y':
                LOGGER.debug(f"[{cond}]관리자 승인 대기중 입니다. 승인 완료 후 안내 메세지가 발송됩니다.")
                BOT.send_message(cid, "관리자 승인 대기중 입니다. 승인 완료 후 안내 메세지가 발송됩니다.")
                make_auth_request_btn(cid)
                return
            elif result_set[0]['dlt_ysno'] == 'N':
                LOGGER.debug(f"[{cond}]이미 등록된 사용자 입니다. 채팅창에 /시작 명령을 입력하여 이용하여 주세요.")
                BOT.send_message(cid, "이미 등록된 사용자 입니다. 채팅창에 /시작 명령을 입력하여 이용하여 주세요.")
                return
            else:
                LOGGER.debug(f"[{cond}]신규유저 등록 절차 진행01!!")
        else:
            LOGGER.debug(f"[{cond}]신규유저 등록 절차 진행02!!")

        result_set, query_str = exec_db(kflow_sess, 'requestUserAuth', cond)
        LOGGER.debug(f"result_set: {result_set}")
        if len(result_set) <= 0:
            LOGGER.error(f"[{cond}]사용자 인증 요청 실패!!")
            _ = make_ret_value(-1, f"[{cond}]사용자 인증 요청 실패!!", 100, len(result_set), result_set, session_list, query_str, "")
            BOT.send_message(cid, "사용자 인증 요청에 실패하였습니다!\n잠시 후 다시 시도하여 주세요.", reply_markup=SENDPHONE)
            BOT.register_next_step_handler(msg, user_auth_process_step)
            return

        _ = make_ret_value(0, f"정상처리 완료", 0, len(result_set), result_set, session_list, query_str, "")

        make_auth_request_btn(cid)
        BOT.send_message(cid, "사용자 인증 요청이 완료 되었습니다. 관리자 승인 후 안내 메세지가 발송됩니다.")

    except AttributeError as e:
        LOGGER.exception(f"전화번호 수집 실패!!")
        msg = BOT.send_message(cid, "사용자 인증 요청에 실패하였습니다!\n잠시 후 다시 시도하여 주세요.", reply_markup=SENDPHONE)
        BOT.register_next_step_handler(msg, user_auth_process_step)
        return


@BOT.message_handler(commands=["start", "시작", "주차"])
def cmd_start(m):
    global PARKINGSET
    LOGGER.debug(f"cmd_start m: {m}")
    cid = m.chat.id
    uname = m.chat.username

    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    cond = {
        "cid": cid,
    }

    is_authed = False

    try:
        result_set, query_str = exec_db(kflow_sess, "findIsAuthedUser", cond)
        LOGGER.debug(f"result_set: {result_set}")
        _ = make_ret_value(0, f"정상처리 완료", 0, len(result_set), result_set, session_list, query_str, "")

        if len(result_set) <= 0:
            is_authed = False
        else:
            if result_set[0]['dlt_ysno'] == 'Y':
                LOGGER.debug(f"[{cond}]관리자 승인 대기중 입니다. 승인 완료 후 안내 메세지가 발송됩니다.")
                BOT.send_message(cid, "관리자 승인 대기중 입니다. 승인 완료 후 안내 메세지가 발송됩니다.")
                make_auth_request_btn(cid)
                return
            is_authed = True

    except Exception as e:
        err_msg = f"DB 처리중 오류가 발생하였습니다!!"
        LOGGER.exception(err_msg)
        return

    if not is_authed:
        noti_msg = """
ParkingAngle For S-CITY서비스에 오신것을 환영합니다.
ParkingAngle 서비스는 사전 등록된 사용자만 사용 가능합니다.
ParkingAngle 서비스는 서비스제공을 위하여 텔레그램 메신져를 이용하며, 이에따른 개인정보 수집, 이용에 관한 사용자 동의가 필요합니다.
동의하지 않으시면 이 채팅방을 나가시면 됩니다.

<개인정보 수집, 이용안내>
1. 수집하는 개인정보 항목: 휴대폰번호, 사용자명
2. 수집목적: ParkingAngle 서비스 제공
3. 보유 및 이용기간: 서비스 종료시까지

ParkingAngle 서비스는 서비스 제공을 위하여 Telegram Messenger LLP에 메세징 업무를 위탁하고 있으며, ParkingAngle 서비스 이용을 위해 개인정보 수집, 이용 안내에 대해서 동의하십니까?
        """
        BOT.send_message(cid, noti_msg)
        msg = BOT.send_message(cid, "개인정보 수집, 이용안내에 동의하시고 사용자 인증을 원하시면 아래 동의 버튼을 눌러주세요.\n원하지 않으시면 이방을 나가시면 됩니다.", reply_markup=SENDPHONE)
        BOT.register_next_step_handler(msg, user_auth_process_step)
    else:
        USER_INFO[str(cid)] = result_set
        BOT.send_message(cid, f"[{uname}]님 원하시는 작업을 선택 하여주세요.", parse_mode="HTML", reply_markup=PARKINGSET)


@BOT.callback_query_handler(func=lambda call: call.data == "btn_cancel")
def inline_btn_btn_cancel(call):
    LOGGER.debug(f"PARA: {call}")
    cid = call.message.chat.id
    uid = call.from_user.id
    uname = call.from_user.username
    mid = call.message.message_id

    retval = remove_msg(cid, mid)
    LOGGER.debug(f"메세지삭제[{cid}/{mid}]: {retval}")

    try:
        BOT.clear_step_handler(call.message)
        
        service_init(cid, uname)
    except Exception as e:
        LOGGER.exception(f"취소 오류!!")


@BOT.callback_query_handler(func=lambda call: call.data[0:call.data.find(DIVIDER)] == "proc_set_phour")
def proc_set_phour(call):
    global GLOBAL_PARAMS
    LOGGER.debug(f"proc_set_phour.call: {call}")
    cid = call.message.chat.id
    uid = call.from_user.id
    uname = call.from_user.username
    mid = call.message.message_id

    params = call.data.split(DIVIDER)
    lnline_name = params[0]
    args = params[1:]

    LOGGER.debug(lnline_name)
    LOGGER.debug(args)
    LOGGER.debug(f"GLOBAL_PARAMS: {GLOBAL_PARAMS}")

    applied_time = GLOBAL_PARAMS[args[0]][args[1]][0]
    car_info = GLOBAL_PARAMS[args[0]][args[1]][1]
    del GLOBAL_PARAMS[args[0]]

    LOGGER.debug(f"applied_time: {applied_time}, car_info: {car_info}, GLOBAL_PARAMS: {GLOBAL_PARAMS}")

    # 인라인 실행시 해당 메세지를 삭제한다.(편의에 따라 추가 삭제)
    retval = remove_msg(cid, mid)
    LOGGER.debug(f"메세지삭제[{cid}/{mid}]: {retval}")

    if not user_auth_check(cid):
        return

    # 할인 등록
    retval = SPA.put_car_disins(int(applied_time), car_info)
    if not retval:
        retval = [0, ]
    LOGGER.debug(f"요청시간: {applied_time}, 미충전 잔량:{retval}")

    # 2022.06.08, helloong, Mod, Fixed bug None type return value error when issue 24 hours ticket.
    if int(applied_time) == 24:
        BOT.send_message(uid, f"요청시간: {applied_time}, 24시간 주차권 지급이 완료 되었습니다.")
    else:
        BOT.send_message(uid, f"요청시간: {applied_time}, 미충전 잔량:{retval[0]}\n{int(applied_time) - int(retval[0])}시간 주차권 지급이 완료 되었습니다.")
    BOT.send_message(uid, f"[{uname}]님 원하시는 작업을 선택 하여주세요.", parse_mode="HTML", reply_markup=PARKINGSET)


@BOT.callback_query_handler(func=lambda call: call.data[0:call.data.find(DIVIDER)] == "proc_reg_car")
def proc_reg_car(call):
    global GLOBAL_PARAMS
    LOGGER.debug(f"proc_reg_car.call: {call}")
    cid = call.message.chat.id
    uid = call.from_user.id
    mid = call.message.message_id

    params = call.data.split(DIVIDER)
    lnline_name = params[0]
    args = params[1:]

    LOGGER.debug(lnline_name)
    LOGGER.debug(args)
    LOGGER.debug(f"GLOBAL_PARAMS: {GLOBAL_PARAMS}")

    car_info = GLOBAL_PARAMS[args[0]][args[1]]
    del GLOBAL_PARAMS[args[0]]
    
    LOGGER.debug(f"car_info: {car_info}")
    # 인라인 실행시 해당 메세지를 삭제한다.(편의에 따라 추가 삭제)
    retval = remove_msg(cid, mid)
    LOGGER.debug(f"메세지삭제[{cid}/{mid}]: {retval}")

    if not user_auth_check(cid):
        return

    # 04. 차량 이미지 조회
    SPA.get_car_image(car_info)

    btn_hours = types.InlineKeyboardMarkup(row_width=2)
    temp_btn = {}
    for x in range(1, 11, 2):
        temp_btn[x] = types.InlineKeyboardButton(f"{x}시간", callback_data="proc_set_phour" + DIVIDER + str(cid) + DIVIDER + str(x))
        GLOBAL_PARAMS[str(cid)][str(x)] = [x, car_info]
        temp_btn[x + 1] = types.InlineKeyboardButton(f"{x + 1}시간", callback_data="proc_set_phour" + DIVIDER + str(cid) + DIVIDER + str(x + 1))
        GLOBAL_PARAMS[str(cid)][str(x + 1)] = [x + 1, car_info]
        btn_hours.add(temp_btn[x], temp_btn[x + 1])
    GLOBAL_PARAMS[str(cid)][str(24)] = [24, car_info]
    btn_hours.row(types.InlineKeyboardButton(f"24시간", callback_data="proc_set_phour" + DIVIDER + str(cid) + DIVIDER + str(24)))
    btn_hours.row(CANCELBTN)

    BOT.send_message(cid, "등록하실 주차 시간를 선택하여 주세요.", parse_mode="HTML", reply_markup=btn_hours)


def proc_get_car_num(m, uid:int = -1, btn_msg_id: int = -1):
    global GLOBAL_PARAMS
    LOGGER.debug(f"##############################################[proc_get_car_num]")
    LOGGER.debug(f"proc_get_car_num.m: {m}")
    cid = m.chat.id
    uname = m.chat.username
    car_num = m.text
    mid = m.message_id

    LOGGER.debug(f"cid: {cid}, uname: {uname}, car_num: {car_num}, mid: {mid}, uid: {uid}, btn_msg_id: {btn_msg_id}")

    retval = remove_msg(uid, btn_msg_id)
    LOGGER.debug(f"메세지삭제[{uid}/{btn_msg_id}]: {retval}")

    try:
        if not user_auth_check(cid):
            return
        
        if (len(car_num) != 4) or (car_num.isdecimal() != True):
            BOT.clear_step_handler(m)
            ret_msg = f"차량번호 4자리가 올바르지 않습니다[입력번호:{car_num}]!\n등록 차량번호 4자리를 입력하여 주세요!"
            msg = BOT.send_message(cid, ret_msg, parse_mode="HTML", reply_markup=CANCELSET)
            BOT.register_next_step_handler(msg, lambda m: proc_get_car_num(m, uid, msg.id))
        
        ret_car_list = SPA.get_car_list(car_num)
        LOGGER.debug(f"ret_car_list: {ret_car_list}")
        if len(ret_car_list) <= 0:
            ret_msg = f"차량번호[{car_num}]로 주차된 차량이 없습니다.\n등록 차량번호 4자리를 입력하여 주세요!"
            msg = BOT.send_message(cid, ret_msg, parse_mode="HTML", reply_markup=CANCELSET)
            BOT.register_next_step_handler(msg, lambda m: proc_get_car_num(m, uid, msg.id))
            return None

        btn_set = types.InlineKeyboardMarkup()
        GLOBAL_PARAMS[str(cid)] = {}
        for idx, key in enumerate(ret_car_list):
            LOGGER.debug(f"idx: {idx}, key: {key}, key[0]: {key[0]}, key[2]: {key[2]}")
            btn_caption = key[0] + " / " + key[2]
            LOGGER.debug(f"btn_caption: {btn_caption}")
            btn_callback = "proc_reg_car" + DIVIDER + str(cid) + DIVIDER + key[0]
            GLOBAL_PARAMS[str(cid)][key[0]] = key
            LOGGER.debug(f"btn_callback: {btn_callback}")
            btn_car_list = types.InlineKeyboardButton(btn_caption, callback_data=btn_callback)
            btn_set.add(btn_car_list)
        btn_set.row(CANCELBTN)

        BOT.send_message(cid, "등록하실 차량 번호를 선택하여 주세요.", parse_mode="HTML", reply_markup=btn_set)
    except Exception as e:
        LOGGER.exception(f"[proc_get_car_num]차량번호 입력 오류!!")
        ret_msg = f"차량번호[{car_num}]로 주차된 차량이 없습니다.\n다시 입력 하여주세요!!"
        msg = BOT.send_message(cid, ret_msg, parse_mode="HTML", reply_markup=CANCELSET)
        BOT.register_next_step_handler(msg, lambda m: proc_get_car_num(m, uid, msg.id))
        return None


@BOT.callback_query_handler(func=lambda call: call.data == "reg_parking")
def reg_parking(call):
    LOGGER.debug(f"reg_parking.call: {call}")
    cid = call.message.chat.id
    uid = call.from_user.id
    uname = call.from_user.username
    mid = call.message.message_id

    LOGGER.debug(f"cid: {cid}, uid: {uid}, uname: {uname}, mid: {mid}")

    retval = remove_msg(cid, mid)
    LOGGER.debug(f"메세지삭제[{cid}/{mid}]: {retval}")

    try:
        if not user_auth_check(cid):
            return

        # retval: [('41:1시간무료(웹) / 잔여수량 999742', '999742'), ('61:1시간유료(웹) / 잔여수량 68', '68'), ('63:2시간유료(웹) / 잔여수량 0', '0'), ('75:24시간유료(웹) / 잔여수량 30', '30')]
        remain_tickets = SPA.get_main()
        remain_info = ""

        if len(remain_tickets) > 0:
            for item in remain_tickets:
                remain_info += f"{item[0]}\n"

        ret_msg = f"{remain_info}등록 차량번호 4자리를 입력하여 주세요!"
        msg = BOT.send_message(uid, ret_msg, parse_mode="HTML", reply_markup=CANCELSET)
        BOT.register_next_step_handler(msg, lambda m: proc_get_car_num(m, uid, msg.id))
    except Exception as e:
        LOGGER.exception(f"[reg_parking]차량번호 입력 오류")


def proc_show_parking(m, uid=-1, btn_msg_id=-1):
    LOGGER.debug(f"##############################################[proc_show_parking]")
    LOGGER.debug(f"proc_show_parking.m: {m}")
    cid = m.chat.id
    uname = m.chat.username
    car_num = m.text
    mid = m.message_id
    today = datetime.now().strftime("%Y-%m-%d")

    LOGGER.debug(f"cid: {cid}, uname: {uname}, car_num: {car_num}, mid: {mid}, today: {today}, uid: {uid}, btn_msg_id: {btn_msg_id}")

    retval = remove_msg(uid, btn_msg_id)
    LOGGER.debug(f"메세지삭제[{uid}/{btn_msg_id}]: {retval}")

    try:
        if not user_auth_check(cid):
            return
        if not SPA.check_isalive():
            LOGGER.debug(f"사이트 연결 확인 실패!!")
            return
        
        if (len(car_num) != 4) or (car_num.isdecimal() != True):
            BOT.clear_step_handler(m)
            ret_msg = f"차량번호 4자리가 올바르지 않습니다[입력번호:{car_num}]!\n조회 차량번호 4자리를 입력하여 주세요!"
            msg = BOT.send_message(cid, ret_msg, parse_mode="HTML", reply_markup=CANCELSET)
            BOT.register_next_step_handler(msg, lambda m: proc_show_parking(m, uid, msg.id))
    
        ret_dis_list = SPA.get_report(today, today, car_num)
        LOGGER.debug(f"ret_dis_list: {ret_dis_list}")
        if len(ret_dis_list) <= 0:
            ret_msg = f"차량번호[{car_num}]로 발행된 할인 내역이 없습니다.\n조회 차량번호 4자리를 입력하여 주세요!"
            msg = BOT.send_message(cid, ret_msg, parse_mode="HTML", reply_markup=CANCELSET)
            BOT.register_next_step_handler(msg, lambda m: proc_show_parking(m, uid, msg.id))
            return None
        
        msg_dis_list = ""
        for idx, item in enumerate(ret_dis_list):
            msg_dis_list += f"{str(idx + 1)}.{item[0]}/{item[1][-8:]}/{item[2]}\n"

        BOT.send_message(cid, HD + msg_dis_list + FT, parse_mode="HTML")

        BOT.send_message(cid, f"[{uname}]님 원하시는 작업을 선택 하여주세요.", parse_mode="HTML", reply_markup=PARKINGSET)

    except Exception as e:
        LOGGER.exception(f"[proc_show_parking]차량번호 입력 오류!!")
        ret_msg = f"차량번호[{car_num}]로 발행된 할인내역이 없습니다.\n조회 차량번호 4자리를 입력하여 주세요!"
        msg = BOT.send_message(cid, ret_msg, parse_mode="HTML", reply_markup=CANCELSET)
        BOT.register_next_step_handler(msg, lambda m: proc_show_parking(m, uid, msg.id))
        return


@BOT.callback_query_handler(func=lambda call: call.data == "show_parking")
def show_parking(call):
    LOGGER.debug(f"show_parking.call: {call}")
    cid = call.message.chat.id
    uid = call.from_user.id
    uname = call.from_user.username
    mid = call.message.message_id

    retval = remove_msg(cid, mid)
    LOGGER.debug(f"메세지삭제[{cid}/{mid}]: {retval}")

    try:
        if not user_auth_check(cid):
            return
        if not SPA.check_isalive():
            LOGGER.debug(f"사이트 연결 확인 실패!!")
            return

        ret_msg = "조회 차량번호 4자리를 입력하여 주세요!\n[당일 주차권 지급 내역만 확인 됩니다.]"
        msg = BOT.send_message(uid, ret_msg, parse_mode="HTML", reply_markup=CANCELSET)
        BOT.register_next_step_handler(msg, lambda m: proc_show_parking(m, uid, msg.id))
    except Exception as e:
        LOGGER.exception(f"[show_parking]차량번호 입력 오류")


if __name__ == '__main__':
    telegram_polling()
